\label{sec:pt-snowflake}

While we continue to support and maintain currently deployed pluggable
transports, we must also focus our efforts on new ones.
Snowflake~\cite{snowflake}, which builds on
Flashproxy~\cite{flashproxy-pets12}, is currently the most promising of these
next generation pluggable transports.

Snowflake sends traffic through temporary proxies called \emph{snowflakes} that
are created within volunteer users' browsers when they visit particular
websites. A snowflake provides a connection from a Tor client (such as Tor
Browser) to a Tor bridge, enabling the person in the censored location to
access the Tor network. A Snowflake client, running with the Tor client, makes
a domain-fronted~\cite{domain-fronting} request to the Snowflake \emph{broker}.
The broker replies with the IP address and other metadata needed for the client
to connect to a snowflake. The client then connects directly to the snowflake
using WebRTC, a peer-to-peer protocol implemented in most modern web browsers.
The snowflake relays traffic between the Tor client and the Tor bridge.
Figure~\ref{fig:snowflake-arch} illustrates Snowflake's architecture.

\begin{figure}
\begin{center}
\fbox{\includegraphics[width=0.8\textwidth]{snowflake-schematic.png}}
\end{center}
\caption{Snowflake architecture~\cite{Fifield2017a}}
\label{fig:snowflake-arch}
\end{figure}

\section{Document the current broker implementation \bluesmallcaps{completed}}

An experimental Snowflake prototype is currently integrated in Tor Browser's
alpha releases for Mac OS and Linux, but much work remains to make it ready for
broad release~\cite{snowflake-eval}. As a starting point for necessary design
improvements, we have examined and documented the current broker
implementation. 

\url{https://github.com/ahf/snowflake-notes/blob/master/Broker.markdown}

\maybeurl{https://bugs.torproject.org/28848}

\section{Improve the Snowflake Protocol}

We plan to redesign the Snowflake protocol to make it extensible and to enable
us to collect useful metrics. This section outlines ideas and questions for
this redesign.

See also: \url{https://github.com/ahf/snowflake-notes/blob/master/Protocol.markdown}

\subsection{Client to snowflake proxy protocol}

A Snowflake client connects to a snowflake to send traffic to a Tor bridge and
then on to the Tor network. We want to add the ability to send some control
messages between the client and snowflake.

\maybeurl{https://bugs.torproject.org/29206}

\emph{Notes:} 
\begin{itemize}

	\item Would it be useful for a client to be able to connect to multiple
	snowflakes? Does this make sense given Tor's current pluggable transport
	design where the Tor client produces only one output stream?

	\item Should the client be able to specify, from a set of bridges, which
	bridge the snowflake should use?

\end{itemize}

\subsection{Snowflake to broker protocol}

Currently snowflakes do a \emph{long poll} to the broker. A long poll in this
context is an HTTP request where the server reads the request, but doesn't send
a reply until it has something to say. We should consider, instead, a
bi-directional communication link between snowflakes and brokers.

Adding more brokers should alleviate concerns about these links resulting in
too many connections for the broker. We could also consider building a back-out
mechanism into the broker-snowflake protocol that would allow the broker
to signal an overly eager snowflake to wait before attempting to connect again.

\maybeurl{https://bugs.torproject.org/29207}

\emph{Notes:}
\begin{itemize}

	\item Tor relays have an identity key, which allows us to reason about
	their performance and stability. Should we (optionally?) allow snowflakes
	to have identity keys? This would allow us to assess snowflakes over time
	to identify which are more stable than others.

	\maybeurl{https://bugs.torproject.org/29260}

	\item For extensibility: allow snowflakes to identify which transport they
	are using, and which version of the transport. This makes it so that we can
	extend the protocol to handle transports other than WebRTC.

	\item For extensibility: make sure the snowflake and broker can communicate
	necessary connection information for transport types other than WebRTC.

	\item We should consider using WebSocket for messages between snowflakes
	and the broker. This would give us a versioned, extensible protocol for
	this communication. It would require a WebSocket handler at the broker and
	modifying the snowflakes to make WebSocket connections.
	
	\maybeurl{https://bugs.torproject.org/29736}
	
	\item We might have snowflakes advertize a capacity value. This could be a
	bandwidth value or the number of connections they can handle.
	
\end{itemize}

\subsection{Client to broker protocol}

A Snowflake client asks the Snowflake broker to give it a snowflake to relay
its traffic through.

\emph{Notes:}
\begin{itemize}

	\item Because communication between the client and the broker is domain
	fronted, which is costly, this protocol should be kept to a minimum.

	\item We might want to collect statistics about which countries requests
	are coming from. If we later see more requests than successful connections
	to snowflakes from a particular location, we can infer that domain fronting
	works from that location, but WebRTC (for example) is blocked.

	\item For extensibility: allow clients to request different transport types
	and transport version numbers.

\end{itemize}

We want to design this protocol so that it is not Snowflake specific. A client
should be able to connect to the broker to request any kind of bridge.
Eventually, the broker might be able to replace the BridgeDB service.

We should explore using various options for connecting to the broker:

\begin{itemize}

	\item IP address: using an IP address rather than a domain name in
	conjunction with a CDN increases collateral damage by forcing the censor to
	block everything behind the IP.

	\item AMP cache fronting: use Google Accelerated Mobile Pages (AMP) caches
	as a front for the broker.

	\item DNS tunneling: tunnel requests to the broker over DNS.

\end{itemize}

\maybeurl{https://bugs.torproject.org/29293}

\subsection{Broker to broker protocol}

Currently, we have a single Snowflake broker. If that broker were to become
unavailable, it would be impossible for snowflakes to announce themselves and
for clients to find snowflakes. To make the system resilient to the failure of,
or an attack on, the broker, we should consider adding more brokers.

An architecture with multiple brokers would require a protocol for the brokers
to reach consensus about which snowflakes are currently available to handle
clients. This would likely require each broker to share some metadata about
their clients with the other brokers. We should initially make this consensus
scheme simple and expand it when we have more experience with the Snowflake
system as a whole.

This protocol is lower priority than the others in this section.

\subsection{Better resilience against Dos attacks for the broker}

The broker needs to be more resilient against DoS attacks. In addition to
traditional DoS techniques (for example SYN floods, packet floods, memory
exhaustion), the broker might be flooded with low-bandwidth snowflakes. An
attacker could create a large number of low resource snowflakes, making it
likely that these would be distributed to clients, degrading the user
experience. We should consider devising a way to test that a snowflake make a
reasonable level of bandwidth available before the broker will distribute it to
a client.

\maybeurl{https://bugs.torproject.org/25593} \maybelinebreak
\maybeurl{https://bugs.torproject.org/25681}

\section{Useful metrics}

We need to collect some information to better understand where Snowflake users
are located, where volunteer snowflakes are located, what kind of load the
various components experience, and pain points in the system. Aside from
helping us understand the operation of the system, this information would help
us detect and respond to censorship events.

\begin{itemize}

	\item Which countries do connections to snowflakes come from? We use
	IP geolocation to determine this. \bluesmallcaps{completed}

	\maybeurl{https://bugs.torproject.org/29734}

	\item Which countries do connections to brokers to request snowflakes come
	from? We use IP geolocation to determine this. \bluesmallcaps{completed}

	\maybeurl{https://bugs.torproject.org/29734}

	\item How much bandwidth use do we see at snowflakes? At bridges?

	\item How frequently does a snowflake fail to connect to a bridge?

	\item What are the locations of snowflakes?

	\item How persistent and reliable are snowflakes?

\end{itemize}

We would also like to publish real-time information about how many snowflakes
are currently registered with the broker and available to serve users. This
would help snowflake volunteers understand whether they are needed and whether
more snowflakes would currently be useful. It would also help censored users
understand the health of the current snowflake population and potentially help
them investigate and solve problems they may have with the system.

In addition, to better understand the address distribution algorithm, we should
consider publishing statistics about how frequently a particular snowflake has
been given to a user, along with how frequently the broker has decided not to
give a user a particular snowflake because it has already been distributed too
many times.

https://bugs.torproject.org/21315

\section{Build Snowflake into Tor Browser}

This section describes several areas of work we need to do to create a viable
version of Snowflake that we will integrate into the main Tor Browser release.

\maybeurl{https://bugs.torproject.org/19001}

\subsection{Reproducible builds}

We need to examine Snowflake's dependencies and analyze how difficult it will
be to integrate into Tor's build processes.

\maybeurl{https://bugs.torproject.org/28672} \maybelinebreak
\maybeurl{https://bugs.torproject.org/25483}

\subsection{Include libwebrtc license files in software release}

We need to ensure that we include the appropriate Chromium license files for
the WebRTC library in our software release. If we decide to use another
library, we need to include the appropriate license files for that library.

\maybeurl{https://bugs.torproject.org/19315}

\subsection{Add Snowflake to Tor Launcher}

We need to add Snowflake to Tor Launcher so that a user can select Snowflake
from the pluggable transport menu.

\section{More Snowflake bridges}

The current implementation of Snowflake's client-side proxy hard codes the URL
of the Snowflake bridge. To provide more resiliency and to support more load, we
need to add more Snowflake bridges. To do this we need to consider what changes
will be necessary at the Snowflake components: the client-side proxy, the
snowflake, and the broker.

\emph{Notes:}
\begin{itemize}

	\item A snowflake might send traffic to any bridge or it may want to
	specify which bridges it is willing to send to.

	\item A snowflake might only send traffic to one bridge. If a snowflake
	sends to multiple bridges, the client needs to have a way to tell the
	snowflake which bridge it wants to use.

	\item The broker needs to learn from snowflakes which bridge or bridges
	they use and from the client which bridge it wants to use. The broker
	needs to be able to match clients with snowflakes that can forward traffic
	to an appropriate bridge.

	\item The client needs to be able to tell the broker which bridge it wants
	to use. It might also need to tell the snowflake which bridge it wants to
	use.

\end{itemize}

In order to support this setup, we need to bring more Snowflake bridges online.
We should recruit trusted members of the Tor community to run this first set of
Snowflake bridges. The bridges should be capable of a high uptime and be
located in areas where censorship is not a big concern.

\maybeurl{https://bugs.torproject.org/28651}

\section{Easier ways to run a snowflake}

We are in the process of revamping the web page a volunteer visits to make
their browser a snowflake. User experience is important here so that the
volunteer understands what is required (WebRTC, javascript), and can see when a
user is connected to their snowflake.

\maybeurl{https://bugs.torproject.org/27385}

A drawback to running a snowflake in a browser tab is that browsers try to
minimize work when a user is not interacting with a tab. This may become a
problem because WebRTC is not generally considered something that needs to run
in the background the way, for example, music streaming is.

Another approach is to create a browser extension (or ``add-on'') to run
snowflakes. Installing and enabling the extension would make the browser act as
a snowflake. The volunteer wouldn't have to keep a tab open and snowflakes
would be more long lived.

Cupcake~\cite{cupcake} is software that makes it easy to distribute and run
Flashproxy~\cite{flashproxy-pets12}, Snowflake's predecessor. Cupcake is
distributed either as a Chrome or Firefox add-on or as a module, theme, or app
on popular web platforms. The latter causes visitors to the site to become
proxies. We are investigating using Cupcake as a browser add-on for snowflakes
and UX discussions for this idea have begun. Cupcake has about 4,000 users on
Chrome, so offers a promising way to get people to run snowflakes.

\maybeurl{https://bugs.torproject.org/23888}

With some publicity, we can probably create a community of volunteers who would
take pride in helping censored users. We frequently hear from people who want
to contribute to the Tor Project but who do not have the resources or skills to
run relays. This would be a great way to get those people involved. We should
consider ways to gamify running a snowflake to show appreciation and make it
more fun.

\maybeurl{https://bugs.torproject.org/20813}

\section{Snowflake connection multiplexing}

\subsection{One snowflake supports multiple clients}

Currently, each snowflake can handle one client at a time. This is fine if the
number of snowflakes is always much bigger than the number of clients. It
would be better if each snowflake could handle multiple clients at the same
time, and we should implement that now so that we're ready when we have a spike
in clients.

\maybeurl{https://bugs.torproject.org/25601}

\subsection{One client uses multiple snowflakes}

Currently, each client sends traffic through a single snowflake. If that
snowflake is slow, performance is degraded. We would like to allow a client
to send traffic through multiple snowflakes. This would require implementing a
sequencing and reliability layer, possibly with a retransmission mechanism. 

A potential privacy benefit is that each snowflake would only see a subset of
the client's traffic. A potential downside is that making a number of
simultaneous WebRTC connections might help a censor learn that Snowflake is
being used.

\maybeurl{https://bugs.torproject.org/25723}

\section{Improve safety for users}

\subsection{End-to-end confidentiality for client registration}

A Snowflake client uses domain fronting to connect to the broker. The
connection to the front domain uses TLS, as does the connection from the front
domain to the broker. But the fronting service itself can see the request. It
is unavoidable that the fronting service can see the IP address of the client,
but we should protect the other metadata that appears in the registration
request by encrypting it. 

We could use a similar solution to that of Flashproxy~\cite{flashproxy-pets12}.
There, the facilitator had a private RSA key and client registration methods
were encrypted before being posted to the facilitator. Key material was
isolated into a facilitator-reg-daemon process that was separated from the web
server and facilitator CGI.

\maybeurl{https://bugs.torproject.org/22945}

\subsection{Sanitize Snowflake logs \bluesmallcaps{completed}}

In certain error situations, for example, when the WebSocket server panics,
client IP addresses are written to Snowflake logs. Because this compromises
user anonymity, we prevent this from happening. We implemented a
log scrubber that uses regular expressions to match IP address patterns so that
we can avoid writing them to the logs.

Several Snowflake components produce log files, so we needed to ensure that we
fixed this issue for all of them: the client, the server, the broker, and the
proxy.

\maybeurl{https://bugs.torproject.org/21304} \maybelinebreak
\maybeurl{https://bugs.torproject.org/30125}

\subsection{Fingerprinting resistance}

In order to defeat DPI, Snowflake's traffic patterns need to be as
indistinguishable as possible from regular WebRTC traffic. We need to analyze
WebRTC's network layer so that we understand how both DataChannel traffic and
MediaStream traffic look and compare that to how Snowflake traffic looks.

\section{An improved WebRTC library}

In addition to data channels for communication between peers, WebRTC comes with
a variety of audio and video capabilities that include various codecs.
Snowflake only uses the peer-to-peer data communication channels, so building a
smaller WebRTC library that does not include the audio/video functionality
might be useful. At the same time, we need to analyze whether other WebRTC
applications use the library this way; we don't want to make Snowflake easily
distinguishable from allowed uses of WebRTC.

\maybeurl{https://bugs.torproject.org/19569}

A pure Go WebRTC implementation is available from Pions~\cite{pions}, so we
might evaluate whether that would meet our needs.

\maybeurl{https://bugs.torproject.org/28942}

We should also consider using Firefox's WebRTC implementation instead of
Chrome's, which would solve the problems with creating a reproducible build on
Windows. The drawbacks, however, may outweigh the benefit: using a headless
browser is difficult, which is why meek was recently modified to stop doing so;
and Tor Browser disables WebRTC to avoid leaks of IP addresses.

\maybeurl{https://bugs.torproject.org/29205}

\section{Test environments}

\subsection{Better test coverage}

To make sure Snowflake is reliable, we need to ensure that our code has a high
degree of test coverage. Part of this will be accomplished by including it in
our Chutney continuous integration process.

\maybeurl{https://bugs.torproject.org/29024}

\subsection{An automated local test environment \bluesmallcaps{completed}}

We created a locally networked testing environment for Snowflake that can
easily and automatically be set up and run. This includes easy installation and
configuration of dependencies. We are using networked docker containers for
this environment.

The goal for this environment is to enable us to reproduce bugs that have
occurred in the deployed system that we had not been able to reproduce locally.

In the future, it might be interesting to see if we can do some kind of
continuous integration with gitlab:
\url{https://docs.gitlab.com/ee/ci/#gitlab-cicd-for-docker}.

\maybeurl{https://bugs.torproject.org/29489}

\subsection{Test suite for NAT topologies}

One of the strengths of Snowflake (as opposed to Flashproxy) is that WebRTC
allows incoming connections to computers that are subject to NAT---a very
common situation. To be sure that Snowflake works for censored users in a
variety of NAT configurations, we need a test suite that tests against various
NAT topologies.

\maybeurl{https://bugs.torproject.org/25595}

\section{Add disk space monitoring}

We need to set up disk space monitoring at both the broker and the default
Snowflake bridge to avoid situations where full disks prevent Snowflake from
working. This falls under the umbrella of adding monitoring for all of our
ant-censorship infrastructure.

\maybeurl{https://bugs.torproject.org/30152} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29863}

\section{Address known issues}

We have identified several known issues that it is a priority to investigate
and address.

\subsection{snowflake-client needs to stop using my network when I'm not giving it requests}

When the user turns Snowflake off, the Snowflake client continues to contact
the broker every 10 seconds to ask for a snowflake, resulting in unnecessary
network use and thus hurting scalability. The Snowflake client needs to be more
defensive by going dormant either after some period during which it has not
received a request or whenever it isn't handling a request.

\maybeurl{https://bugs.torproject.org/21314}

\subsection{Need something better than client's `checkForStaleness`}

We currently have code in place to check the staleness of a connection between
a client and a snowflake to prevent long-lived, broken connections from
persisting. But because there is no heartbeat message at this layer of
abstraction, pauses in use, such as the user reading a webpage, cause a
disconnection after 30 seconds. We need to solve the broken connection problem
in a way that does not result in these repeated disconnections.

\maybeurl{https://bugs.torproject.org/25429}

\subsection{proxy-go is still deadlocking occasionally \bluesmallcaps{completed}}

Our test snowflakes were hanging after they had been running for a few days.
All of the snowflakes exhibited this behavior, but the more heavily used
snowflakes exhibited it first.

Our code incorrectly assumed that the OnIceComplete callback was only called
when the connection between the client and the snowflake was complete. That
wasn't the case, so the snowflake thought it had a connection with the client
when it didn't. Since a proxy-go instance can only handle a certain number of
clients at a time, it slowly used up all the available slots.

We implemented two things to fix this. First, we have a channel that one Go
routine uses to signal that a connection is complete. Second, if the signal has
not been received within a period of time, we do an explicit check to see if
the connection is complete.

\maybeurl{https://bugs.torproject.org/25688}

\subsection{Guard against large reads \bluesmallcaps{completed}}

The broker sends and receives relatively small messages when it communicates
with the clients and the snowflakes. We have limited the resources we allocate
for these messages so that malformed messages don't consume excess memory.

\maybeurl{https://bugs.torproject.org/26348}
