\label{sec:bridges}

Some censors attempt to block access to the Tor network by blocking the Tor
relays listed in the public directory of relays. Tor bridges are relays that
are not listed in the public directory. This makes bridges harder to block than
regular relays, but creates a challenge: how can we distribute bridge addresses
to users while keeping them from censors? There are currently a variety of ways
for users to discover bridges: some bridges come pre-configured in Tor Browser;
users can request bridges using an ordinary web browser or by email; in Tor
Browser 8.0 and later, the moat feature allows a user to request a bridge from
within the browser settings and have it configured automatically. In all of
these cases, the bridge addresses come from BridgeDB, a database that stores
addresses of all of the currently known public bridges.

%Because anyone can download Tor Browser, censors can discover the bridges that
%come pre-configured relatively easily. If the pre-configured bridges don't work
%in a particular location, users can request more bridge addresses from
%BridgeDB. Staring with
%Tor Browser 8.0, users can request bridges from within the application
%settings. People using earlier version can request bridge addresses directly
%from the BridgeDB service.

Several areas of work to improve BridgeDB are detailed below.

\section{Improve BridegDB's user experience}

\subsection{Localization}

We need to localize all aspects of the BridgeDB user flow. In some cases, this
will involve investigating why localization that has already been implemented
is not working. An example of this is that response emails that should be
offered in non-English languages are not. In addition, we need a language
switcher on the https://bridges.torproject.org so that users can override
geolocation.

\maybeurl{https://bugs.torproject.org/26543} \maybelinebreak
\maybeurl{https://bugs.torproject.org/15404}

\subsection{Improve CAPTCHA usability}

Users requesting bridge addresses from the BridgeDB web interface are required
to solve a CAPTCHA before receiving bridge addresses. Our current CAPTCHA
system often provides images that contain letters that are very difficult for
humans to discern. In addition, we do not currently provide audio CAPTCHAs that
would enable users with visual impairments to request bridge addresses from the
web interface. We need to address these issues to make BridgeDB's web interface
usable for more people.

\maybeurl{https://bugs.torproject.org/24607} \maybelinebreak
\maybeurl{https://bugs.torproject.org/10831}

\section{Investigate design improvements}

\subsection{Improve the current BridgeDB implementation}

We need to consider ways to improve the design of the BridgeDB service. First
we need to assess current usage patterns such as which distribution mechanisms
are being used, how much each is being used, and where requests are coming
from. As part of this work, we should specifically consider whether Yahoo's
account creation protections have been weakened and thus whether we should
continue to accept requests from that domain.

\maybeurl{https://bugs.torproject.org/28496}

We should also consider additional domains to accept email requests from,
taking into account which censored locations need non-default bridges to
connect to the Tor network. 

Salmon~\cite{salmon-pets2016} offers a proxy distribution method that employs
an algorithm to automatically identify and prevent malicious users from learning bridge information. We should
evaluate whether we could use Salmon to improve how BridgeDB distributes
bridges.

\maybeurl{https://bugs.torproject.org/29288}

\subsection{Add automated monitoring}

The BridgeDB service needs automated monitoring to ensure availability of all
aspects of the service. This should include whether the email request mechanism
is working, whether it gets updated, and whether the bridge authority is
reachable. This falls under the umbrella of adding monitoring for all of our
anti-censorship infrastructure.

\maybeurl{https://bugs.torproject.org/12802} \maybelinebreak
\maybeurl{https://bugs.torproject.org/30152}

\subsection{Monitor the bridge authority \bluesmallcaps{completed}}

The bridge authority is currently monitored by sysmon.

\maybeurl{https://bugs.torproject.org/29229}

\section{Assess and improve the email distribution mechanism}

We need to assess whether BridgeDB's email request mechanism is working
correctly, and consider ways to improve it. One area to address is that an
email reply is not sent if the help text is quoted in the body of the email
request.

\maybeurl{https://bugs.torproject.org/17626}

We should consider partitioning the set of bridges so that BridgeDB distributes
distinct subsets in response to requests from each email domain. This would
prevent a subverted provider from learning the complete list of bridge
addresses. However, this enhancement might also allow an observer to learn a
user's email provider by observing which bridge they connect to. We need to
consider how to accomplish the goal of protecting bridges while also protecting
user privacy.

\maybeurl{https://bugs.torproject.org/11330}

\section{Current implementation: status and cleanup \bluesmallcaps{completed}}

There are a few areas where we cleaned up BridgeDB's configuration and
start-up system:

\begin{itemize}

	\item We added a README file to document BridgeDB administration, including
	how the repository is organized, as well as how the current instance is
	configured. This makes it easy to set up another instance if necessary.
	
	\item We cleanup up bridgebd.crontab, removing unused lines, cleaning up
	comments, and otherwise better documenting the configuration.

	\item We cleaned up bridgedb.conf, removing outdated configuration
	entries.

	\item We added a systemd initialization script to facilitate starting and
	reloading bridgedb.

\end{itemize}

\maybeurl{https://bugs.torproject.org/29481} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29273} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29483}

\subsection{Address continuous integration error \bluesmallcaps{completed}}

We use the Travis continuous integration system for BridgeDB. We closed a ticket
concerning a temporary error that is no longer happening.

\maybeurl{https://bugs.torproject.org/26154}

\section{Improve BridgeDB deployment}

\subsection{Create a software release for BridgeDB}

Historically, BridgeDB has been deployed and run by the BridgeDB developers. It
would be better to decouple the development from the deployment and
administration, so we want to create a software release for BridgeDB. In
addition, we could make the release available so that NGOs, for example, could
deploy their own instance of BridgeDB to distribute private bridges to their
members and allies inside a censored country. For more information on making it
easier for NGOs to use private bridges, see Section~\ref{sec:bridges-for-ngos}.

\maybeurl{https://bugs.torproject.org/29276}

\subsection{Document BridgeDB Infrastructure \bluesmallcaps{completed}}

We documented the configuration of the currently running BridgeDB so that
we can replicate it when needed.

\maybeurl{https://bugs.torproject.org/29273}

\section{Consider alternative methods of bridge distribution}

The traditional way of distributing bridge information is for BridgeDB to give
out bridge connection information that is then added to the Tor Browser
configuration either manually or automatically.

Snowflake and meek handle bridge information differently at the client side,
and we might like to consider a broader application of the Snowflake broker
style of distributing bridges. To do so, we need to assess what changes would
be required at the client side to make this work.

\maybeurl{https://bugs.torproject.org/29296}

\section{Distribute IPv6 bridges}

BridgeDB does not currently distribute IPv6 bridge addresses, but IPv6
reachability testing is enabled from the bridge authority and Tor Metrics
collects information about IPv6 bridges. We need to investigate why BridgeDB
does not distribute these bridges and fix the issue.

\maybeurl{https://bugs.torproject.org/26542}

\section{Protect bridges with robust pluggable transports \bluesmallcaps{completed}}

Many Tor bridges can be used either as regular bridges or as bridges with
pluggable transports. In these cases, the pluggable transport is reachable on
the same IP address as the regular bridge, but on a different port. As we
discussed in Interim Report 1~\cite{tor-2018-11-001}, obfs4 is not susceptible
to the active probing attacks that regular bridges and older pluggable
transports are. This makes bridges running an obfs4 pluggable transport
valuable in certain censored locations that routinely use active probing, such
as China. 

Recent research~\cite{Fifield2017a, Wang2017a, Dunna-foci18} has indicated that
China has moved from blocking bridges by IP address and port to blocking all
connections to a bridge's IP address. This means that when a user in China
requests a regular bridge or a bridge with a pluggable transport that is not
resistant to active probing, when they try to use it, the censor will learn
that it is a bridge and block its IP address. If that bridge also offers the
obfs4 pluggable transport on a different port, it will be blocked as well. This
is an unfortunate result because of the value of obfs4 bridges for evading
censorship in China.

To protect bridges running obfs4 from being discovered and blocked in this way,
if a bridge offers obfs4, BridgeDB only distributes its connection information
(that is, the IP address and port) when an obfs4 bridge is requested.

\maybeurl{https://bugs.torproject.org/28655}

\section{Collect and export statistics}

In order to understand how BridgeDB is being used and how we might improve the
service, we need to collect statistics. Useful metrics might include the number
of requests, the numbers of requests from each country, and the frequency with
which each distributor is being used. A first step in completing this work is
to evaluate what statistics we are already collecting and how these are meeting
our needs.

\maybeurl{https://bugs.torproject.org/9316} \maybelinebreak
\maybeurl{https://bugs.torproject.org/29269}

Specifically, we would like to know how many bridges exist for each pluggable
transport. We might be able to collect this information from within BridgeDB or
we may prefer to have BridgeDB expose the information in a format that
CollectTor~\cite{collector} can use to integrate the information into our Tor
Metrics website.

There are several considerations to address in terms of how we should count the
bridges: 

\begin{itemize}
	
	\item Should we only count bridges that have the \emph{running} flag? Only
	ones reachable by OONIProbe?

	\item How should we sanitize the numbers? Round them?  Provide a range?
	
	\item Do we care about the number of bridges that offer a particular
	transport or the number of instances?

\end{itemize}

\maybeurl{https://bugs.torproject.org/14453}

