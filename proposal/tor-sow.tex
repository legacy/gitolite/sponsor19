
% \subsection*{Defending communication (Tasks 6-11)}

Around the world, people are trying to safely speak out about their
situations, and safely learn what others have to say. This free and
open communication on the internet is critical to the growth of strong
communities and societies. At the same time, nation-state censors and
others attempt to \emph{deny} this communication, threatening
this growth. In this project we will leverage our past work on the
{\bf{Tor anonymity system}} and the {\bf{pluggable transport ecosystem}}
to explore and analyze techniques to detect, understand, and
mitigate these denials of service.

Safe communication on the internet requires many components to come
together at once: (1)~a robust and highly scaled communications
infrastructure that protects communications metadata (i.e.~Tor);
(2)~mechanisms to get around blocking or censorship of connections between
users and this privacy network; (3)~suitable packages designed for the
computing environments of real users, with an emphasis on usability and
user experience; and (4)~awareness of the changing landscape of threats,
and adaptive user education about these threats.

With these components in mind, we will focus on understanding six research
areas (denoted Tasks 6-11 below). Note
that each of these areas is itself an open-ended research field, so while we
hope that we and others will make substantial progress on each of them,
there will always be
more follow-up work to do on each of them.

While Tor, like other research organizations, performs a variety of
fundamental research for other funders in related topics, we expect that
the {\bf{``information developed under this contract''}} will consist
of interim reports, and a final report, summarizing and analyzing
progress made
by the broader Tor research community on each research area.

%\vspace{-2mm}
% \subsection[Task 6: onion routing network defense]{Task 6: onion routing network defense}
\subsection*{Task 6: onion routing network defense}
%\vspace{-2mm}

\noindent{\em Objective.}
Tor~\cite{tor-design} has a deployed network
and diverse user base that provides a great foundation for this project,
but the network must become
stronger. We want to consider defenses on onion routing networks, both
from improving capacity and from improving underlying design.

\vspace{1mm}
\noindent{\em Approach.}
Strengthening the deployed onion routing network involves:

\noindent {\bf Subtask 6a:}
Fostering diversity and sustainability in relay locations and relay
operators

\noindent {\bf Subtask 6b:}
Reacting as needed to denial of service attacks on the Tor network
itself~\cite{botnet-2013,botnetfc14,load-2017}

\noindent {\bf Subtask 6c:}
Proactively identifying and resolving DoS vulnerabilities, making
use of existing research collaborations~\cite{dos-techreport} to achieve
a stronger network and
improve robustness to unknown DoS vectors.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

%\vspace{-2mm}
%\subsection[Task 7: better pluggable transports
%that work in actively censored regimes]{Task 7:
%better pluggable transports that work in actively censored regimes}
\subsection*{Task 7: better pluggable transports that work in actively censored regimes}
%\vspace{-2mm}

\noindent{\em Objective.}
Currently, domain fronting~\cite{domain-fronting} systems like
meek~\cite{meek} work in most or all denied countries,
but domain fronting scales poorly both in terms of bandwidth
and monetary cost, and also it is fragile in the sense that a small set
of western companies can conspire to significantly degrade it. The most
promising of the next
generation of PTs is Snowflake~\cite{snowflake}, which builds on Flash
Proxy~\cite{flashproxy-pets12} to use in-browser webrtc to make ``Google
Hangout'' style connections to volunteer web browsers, who then bridge
the connections on to the Tor network. We want to have better pluggable
transports that work in actively censored regimes.

\vspace{1mm}
\noindent{\em Approach.}
Research topics in this area include:

\noindent {\bf Subtask 7a:}
Understanding what parts are missing from Snowflake, and how hard it
would be for someone to build a prototype

\noindent {\bf Subtask 7b:}
Understanding barriers to making Tor Browser releases that integrate
such a prototype

\noindent {\bf Subtask 7c:}
Understanding whether a system like Snowflake does indeed work in
denied countries

\noindent {\bf Subtask 7d:}
Learning about barriers to getting pluggable transports like Snowflake
working on Android
too, since an increasing percentage of the world is moving from desktop
to mobile

\noindent {\bf Subtask 7e:}
Keeping informed of other efforts on something to follow Snowflake so
the censorship circumvention world is prepared if attackers become
willing to block or degrade the
webrtc protocol.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

%\vspace{-2mm}
%\subsection[Task 8: build capacity and strengthen the research
%community]{Task 8: build capacity and strengthen the research
%community}
\subsection*{Task 8: build capacity and strengthen the research community}
%\vspace{-2mm}

\noindent{\em Objective.}
Having an actual deployed onion routing testbed means that
most new anonymous communications research focuses on this
network~\cite{research-blog-post}, and this research attention
has triggered an explosion of academic pluggable transport
designs~\cite{tor-pt-spec,pluggable-transports-list}. We want to continue
to build capacity and strengthen the research community.

\vspace{1mm}
\noindent{\em Approach.}
We will
support members of the PETS research community to continue
operation of the
Tor Research Safety Board (a group of researchers who study
Tor, and who want to minimize privacy risks while fostering a better
understanding of the Tor network and its users), with their output being
a growing set of real-world case studies on how to safely and ethically
conduct experiments on the live Tor network and users~\cite{trsb}.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

%\vspace{-2mm}
%\subsection[Task 9: consider more use cases than just web
%browsing]{Task 9: consider more use cases than just web browsing}
\subsection*{Task 9: consider more use cases than just web browsing}
%\vspace{-2mm}

\noindent{\em Objective.}
Smartphone users want privacy and censorship circumvention
for more than just web browsing.
Millions of people use chat and social media applications,
create and share media files like images and video,
and more. In fact, if Tor's performance penalties are most visible for
latency-sensitive applications like web browsing, we would be wise to
explore secure messaging and other asynchronous applications. We want to
consider more use cases than just web browsing.

\vspace{1mm}
\noindent{\em Approach.}
We will:

\noindent {\bf Subtask 9a:}
Understand whether existing prototypes and tools work with Youtube
and/or other popular services

\noindent {\bf Subtask 9b:}
Do a needs assessment for what other applications are popular among
users behind censorship, and how well we can serve them, considering
both safety and usability. The first challenge is to understand what
would actually be useful to build.
Options to consider include designing a video sharing service that
integrates characteristics of SecureDrop~\cite{securedrop} or
Globaleaks~\cite{globaleaks}, or adding an ``upload resume'' or ``parallel
upload'' helper to Tor Browser so users can
upload chunks at a time and not have to start over when the network fails.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

%\vspace{-2mm}
%\subsection[Task 10: understand onion routing client performance on limited
%and/or intermittent connections, and network performance while under
%attack]{Task 10: understand onion routing client performance on limited
%and/or intermittent connections, and network performance while under
%attack}
\subsection*{Task 10: understand onion routing client performance on limited
and/or intermittent connections, and network performance while under
attack}
%\vspace{-2mm}

\noindent{\em Objective.}
Several factors contribute to making Tor connections less efficient than
a direct connection to the final destination~\cite{why-slow}, and while
a big part of that
difference can be explained by how much capacity is available at the Tor
relays (see Task 6), some of these performance factors are particularly
acute on bad network connections~\cite{pets2011-defenestrator}. At the
same time, we need to better
understand how load on the network itself, including adversarially-induced
or targeted load~\cite{torspinISC08}, can impact all parts of the system.

\vspace{1mm}
\noindent{\em Approach.}
We will:

\noindent {\bf Subtask 10a:}
Consider the factors that impact end-to-end performance and
congestion of
Tor clients behind bad network connections (high-latency, low-bandwidth,
some packet loss, etc)

\noindent {\bf Subtask 10b:}
Consider ways to get around these limitations, for example with
the ``resume'' feature mentioned above, or by having clients stripe
their connections and/or circuits over multiple Snowflakes for better
performance and better robustness~\cite{pets13-splitting}

\noindent {\bf Subtask 10c:}
Study the behavior of Tor clients and Tor relays under various
denial of service attacks, and consider countermeasures to tolerate the
attacks and/or gracefully degrade service~\cite{dos-techreport}.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

%\vspace{-2mm}
%\subsection[Task 11: network measurement feedback loops so researchers
%can know what's working and what should work]{Task 11: network
%measurement feedback loops so researchers can know what's working and
%what should work}
\subsection*{Task 11: network
measurement feedback loops so researchers can know what's working and
what should work}
%\vspace{-2mm}

\noindent{\em Objective.}
We want to research network measurement feedback loops because they
will be critical to adapting Tor Browser and other apps in response to
changes on the internet.

\vspace{1mm}
\noindent{\em Approach.}
We will:

\noindent {\bf Subtask 11a:}
Understand barriers to ramping up our OONI-style~\cite{ooni-paper}
censorship measurement tests
in denied areas, so researchers can confirm that various protocols like
webrtc do work right now, and can get rapid and robust notifications
when that changes

\noindent {\bf Subtask 11b:}
Figure out how to conduct more comprehensive tests too: not just
``does webrtc work?''~but ``does Tor Browser using Snowflake work?''

\noindent {\bf Subtask 11c:}
Maintain and grow our public and open Tor Metrics data sets, which provide
historical and ongoing Tor network data and performance
statistics~\cite{wecsr10measuring-tor} for the broader research community.

\vspace{1mm}

\noindent {\bf Milestones and Deliverables.}
Milestones and deliverables will include interim reports, and a final
report, summarizing and analyzing progress on this research area.

