The Tor network is an open network. Tor directory authorities periodically
publish information on the state of the network, including a list of the
addresses of all of the relays that are available for use. This information
must be available so users can use the network, but its availability
allows various actors, such as ISPs or nation states, to prevent users from
connecting to the Tor network. It also allows service providers (like
websites) to block requests coming \emph{from} the Tor network.

% Not sure this is relevant. If they can block guards, why does blocking dirauths matter?
%Since Tor users  need to be able to connect to the Tor network to
%learn information about the current state of the network from the directory
%subsystem, this becomes an effective way to block access to the Tor network
%from users.

Tor bridges are Tor relays that are not listed in the public directory. This
makes it hard for censors to block connections to them. Of course, because
we can't publish a list of bridge addresses, we need another way for users that
need them to discover them.

Using a bridge is often not enough to evade censorship. Some censors use deep
packet inspection (DPI) to prevent people from using Tor. DPI is a technique
that examines data as it moves across the internet and tries to determine what
kind of traffic it is, for example, regular web traffic, encrypted web traffic,
video streaming, or Tor. Pluggable transports try to solve this problem by
disguising the Tor traffic between the user and the bridge so that it doesn't
look like Tor traffic.

\subsection{Bridge distribution}
\label{bridge-distrib}

Because bridge addresses are not listed in the public directory of relays,
applications that use Tor need another way to find them. Tor Browser comes
pre-configured with a few dozen bridge addresses that might help in places where Tor
is blocked. But since anyone can download Tor Browser and see the list, censors
can block these bridges along with blocking the publicly listed relays.

If the bridges that come with Tor Browser don't work, an option for
finding other bridge addresses is \emph{BridgeDB}. Users can access BridgeDB at
https://bridges.torproject.org/ using an ordinary web browser. The user solves
a CAPTCHA and is given some bridge configuration lines, which include the
bridge address.  A user can also request bridges via email. This option is useful if a
censor blocks the BridgeDB website. The request must be made from a Yahoo, GMail,
or Riseup email address and sent to\\bridges@torproject.org.

The BridgeDB service partitions the bridges that it gives out,
with the goal of limiting the rate at which attackers can discover
bridges. Specifically, an attacker who wants to enumerate many bridges
needs to interact with the web interface from many different IP addresses
around the internet, and needs to interact with the email interface from
many different email addresses.

When requesting bridges from BridgeDB, either over the web or by email, a user
can also specify additional information, such as whether they need a specific
pluggable transport or require addresses that work over IPv6.

There are several drawbacks to the BridgeDB solution: the user has to manually
make the request; the user may not know whether they need a pluggable transport
or an IPv6 address; the user has to manually enter the bridge configuration
lines into a Tor Browser configuration dialog.

Tor Browser 8.0, released in September of 2018, includes the ability to request
bridge addresses from within the application. More information about the way
Tor Browser helps get around censorship is in Section~\ref{browser-censorship}.

\subsection{Pluggable transports}
\label{pt-types}

A pluggable transport is a program that is used between the
user's Tor client, such as Tor Browser, and a Tor bridge. The pluggable
transport's job is to change the appearance of the data that flows between
itself and the bridge so that a censor won't be able to recognize it as Tor
traffic. See Figure~\ref{obfsproxy-diag} for an example.

\begin{figure}
\begin{center}
\fbox{\includegraphics[width=0.8\textwidth]{task7a/obfsproxy_diagram.png}}
\end{center}
\caption{Pluggable transport example using obfsproxy}
\label{obfsproxy-diag}
\end{figure}

The pluggable transport API~\cite{tor-pt-spec} enables developers and
research groups to invent pluggable transports that are resistant to different
types of censorship. Then a pluggable transport can be chosen based on the
conditions a user is experiencing in their location. The availability of
\emph{goptlib}, a Go programming language library that takes care of all the
communication between the Tor client and the pluggable transport, has made the
Go language very popular for developing pluggable transports~\cite{goptlib}.

The following sections describe some pluggable transports that either have been
used by Tor, or that have the potential be used by Tor in the future.

\subsubsection{Obfsproxy}

Obfsproxy makes Tor traffic look like a random stream of bytes. This type of
pluggable transport is known as a \emph{look-like-nothing} pluggable transport
and works best when censors have a list of protocols that they try to recognize
and then block. There have been three versions of obfsproxy.

\textbf{obfs2}

Obfs2 was the first pluggable transport that the Tor Project developed.
Inspired by the Obfuscated OpenSSH project, obfs2 simply transforms Tor
traffic into a random stream of bytes. It uses a naive protocol for
exchanging encryption keys, which makes it vulnerable to passive attacks that
can reconstruct the keys and then recognize the obfuscation.

\textbf{obfs3}

Obfs3 is another older pluggable transport very similar to obfs2. obfs3 uses
Diffie-Hellman, a better key exchange protocol, which makes it immune to passive
attacks that try to deduce the encryption keys.

\textbf{obfs4}

All of the pluggable transports before obfs4 were vulnerable to a class of
active attacks called \emph{active probing attacks}. A censor can connect to
a bridge and pretend to be a client using a pluggable transport. If the bridge
replies with pluggable transport traffic, then the censor can tell that it's a
bridge and blocks it immediately. This tactic has been used by China to detect
and block bridges~\cite{greatfirewall-tor}.

Obfs4 is the first deployed pluggable transport protocol to defend against this
attack. Obfs4 requires that the client prove it knows a secret before the
bridge replies. This means obfs4 can resist active probing attacks.

Obfs4 is one of the most popular pluggable transports in use today.

\subsubsection{ScrambleSuit}

ScrambleSuit, like obfsproxy, is a pluggable transport that makes traffic look
like a random stream of bytes. But ScrambleSuit also changes the lengths of its
data packets, as well as the time between sending them, to confuse censors that
rely on those characteristics to recognize Tor traffic.

\subsubsection{FTE}

Format Transforming Encryption (FTE) was one of the first pluggable transports
that did not belong in the look-like-nothing group. Instead, FTE uses an
encryption scheme to make Tor traffic look like regular http web traffic, \ie
it looks like the user is just visiting unencrypted
websites~\cite{fte-usenix14}.

This type of pluggable transport is useful in the face of a ``whitelisting''
censor, that is, a censor that has a list of protocols that it recognizes and
allows.

\subsubsection{Flashproxy}

Flashproxy~\cite{flashproxy-pets12} is a pluggable transport that allows
censored users to quickly find and use short-lived proxies operated by
volunteers around the globe. The strength of the system is that these proxies
can be operated by a volunteer simply visiting a website---no technical skill
is required. Because the user's Tor client connects to one of these browser
proxies rather than a
Tor bridge, there is no need to find bridge addresses. Flashproxy hides its
traffic within Websocket, a common web protocol.

A major weakness of Flashproxy is that the vast majority of censored users are
subject to \emph{network address translation} (NAT) which means they can not
accept incoming connections from the internet. While it's fine for the
volunteer flashproxies to be behind NAT (because they make outgoing
connections to both the censored users and the Tor network, to glue them
together), the limitation that the censored users need to be directly
reachable on the internet drastically reduces the usefulness of the design.

See Section~\ref{pt:snowflake} for a description of Snowflake, an improved
version of Flashproxy that solves this problem.

\subsubsection{Meek}

Meek~\cite{meek} is a pluggable transport that uses \emph{domain
fronting}~\cite{domain-fronting} to get around
censorship. In domain fronting, a client makes a connection to an allowed
website and that website redirects the connection to a censored site that is in
the same domain. This technique is generally used within the domains of large
cloud service providers. Meek hides its traffic within encrypted web requests
and responses between it and the allowed website.

There are two major problems with domain fronting. Cloud providers charge for
all of the traffic that goes into and out of their networks, so it is expensive
to deploy. It also requires the cooperation of the owner of the domain, which
makes it susceptible to political pressure on domain owners. For example, until
April 2018, Amazon and Google supported domain fronting; they disabled it,
reportedly in response to pressure from the Russian
government~\cite{russia-amazon-google}. Currently, the only place where Meek is
deployed is the Microsoft Azure cloud.  

So far, meek has proven resilient to blocking. 

\subsubsection{Marionette}

Marionette is a fairly new pluggable transport developed by the US company
RedJack. Marionette's design~\cite{marionette} is similar to that of
FTE. Marionette can handle a
relatively large amount of traffic quickly and can be used to make Tor traffic
look like traffic from a variety of different protocols. For now, Marionette
is focused on making Tor traffic look like regular web traffic.

Marionette is written in Google's Go programming language and uses the
goptlib library.

Marionette is in the late development stage. The next step is to deploy it to
a set of stable bridges so that we can evaluate it in a variety of censored
locations.

\subsubsection{Next generation pluggable transport: Snowflake}
\label{pt:snowflake}

The most promising next generation pluggable transport is
Snowflake~\cite{snowflake}.
Snowflake is similar to Flashproxy in that it does not require the user to find
bridges, and it connects the user's Tor client to a short-lived proxy. Like
Flashproxy, the proxies are run by volunteers visiting a web page.

Snowflake hides its traffic within the WebRTC protocol---the same protocol that
Google Hangouts uses. The advantage of using WebRTC is that it has the ability
to allow incoming connections to computers using it even if they are subject to
NAT.

Snowflake is a work in progress. Although we have a working prototype in the
alpha releases of Tor Browser for Linux and Mac OSX, there are challenges to
getting it working on Windows. In addition, there are several outstanding
research questions, including understanding what is missing from the code base,
understanding whether a system like Snowflake does indeed work in denied
countries; understanding barriers to getting pluggable transports like
Snowflake working on Android too, since an increasing percentage of the world
is moving from desktop to mobile.

The Guardian Project~\cite{guardianproj} currently
has a grant to begin to answer some of these questions in the context of
Android and Orbot. They also hope to work on enabling any Android device, such
as Chromebooks, Android TVs, and other home devices to act as volunteer
Snowflake bridges.

The Tor Project is in the process of hiring a software developer to
explore some of these questions and to work on other anti-censorship
technologies as well.

\subsection{Designing new pluggable transports}
\label{pt:design}

A common anti-censorship strategy is to make it unappealing for censors to
use a certain blocking method because of \emph{collateral
damage}\textemdash allowed content that will also be blocked. For example, domain fronting works because most censors don't want to
block access to large cloud providers. A variation on this theme is to mimic
traffic that is too important to block, such as web traffic or WebRTC traffic.

In designing new pluggable transports, we need to consider how a censor
might attempt to block them, as well as what collateral damage might result
from that blocking. Adversary
Lab~\cite{adversary-lab} is a service that analyzes captured network traffic to
identify statistical properties. Using this analysis, developers can create
filtering rules to block sampled traffic, and learn how to improve a transport
so that blocking it would maximize collateral damage.
